# find_tar

find_tar aims at combining find, uncompression and unpacking.
This is done recursively, and efficiently (small footprint in RAM, no disk write except caching for unchanged files).

currently, there are two shell scripts:
- updatedb_tar    exploring tar, compressed files, like find -print
- file_sha256_tar exploring tar, compressed files, like find -exec file {} \; -exec sha256sum {} \;

updatedb_tar CACHE_FOLDER:
Outputs list of files in current folder while recursively untarring on the fly.
CACHE_FOLDER is used to keep the result for unchanged tar archives.
updatedb_tar /dev/null barks but works as expected.
One argument is mandatory. If it is --help or -h, this text is shown.

file_sha256_tar CACHE_FOLDER:
This behaves like updatedb_tar CACHE_FOLDER, but uses /usr/bin/file and /usr/bin/sha256sum instead of just echoing the filename.

# See also:

ratarmount, archivemount, GVfs, slocate

# current limitations:

1. updatedb_tar does not update a database looked up by slocate.
2. file_sha256_tar only caches results for archives.
3. find_tar should be created similarily as something with same syntax as find.
4. caching results for archives are desactivated if you give a non-existing directory such as /dev/null/.
5. for newlines in filenames, we should allow to use \000 instead of \012 to separate filenames in outputs. For that, use -print0, | tr '\012\000' '\000\012' | awk [...] | tr '\012\000' '\000\012' instead of awk, ...
6. current version explores $PWD instead of exploring the arguments like find would.
7. it could be a bit faster with dash but dash was not installed by default so I quit.
8. undocumented dependencies (tar recent enough to auto-decompress and to support -x --to-command, file sha256sum zcat must accept --, ...)
9. it could automount or in early versions print mount commands for disk images met, using kpartx and user-space mounting. You can currently spot them using output of file_sha256sum commands.

Feel free to submit pull requests to fix these :-)